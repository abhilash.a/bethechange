package main

import (
	"btc-access-control/services"

	"github.com/gin-gonic/gin"
)

type Details struct {
	UserId     int
	ExpiryTime int
	Uuid       string
}
type token struct {
	value string
}

func main() {
	r := gin.Default()
	r.GET("/token", func(ctx *gin.Context) {
		var det Details
		ctx.BindJSON(&det)
		token, _ := services.GenerateJwtToken(det.UserId, det.ExpiryTime, det.Uuid)
		ctx.JSON(200, gin.H{
			"token": token,
		})

	})
	r.GET("/validatetoken", func(ctx *gin.Context) {
		token := ctx.Request.Header.Get("Authorization")

		resp := services.ValidateJwtToken(token)
		if resp.Valid == true {
			ctx.JSON(200, gin.H{
				"response": resp,
			})
		} else {
			ctx.JSON(400, gin.H{
				"message": resp.ErrorMsg,
			})
		}

	})
	r.Run(":8080")
}
